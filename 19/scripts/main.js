function camelize(str){
  var newStr = "";
  var sym = false;
  for (let i = 0; i < str.length; i++){
    if (str.charAt(i) == '-'){
      sym = true;
      continue;
    }
    if (sym){
      newStr += str.charAt(i).toUpperCase();
      sym = false;
      continue;
    }
    newStr += str.charAt(i);
  }
  return newStr;
}

document.addEventListener("DOMContentLoaded", function(e) {
  elem = document.getElementById('btn');
  elem.onclick = function(){
    var inputField = document.getElementById('inputField');
    var outputField = document.getElementById('outputField');
    var inputValue = inputField.value;
    var newValue = camelize(inputValue);
    outputField.value = newValue;
  }

})
