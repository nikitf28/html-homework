document.addEventListener("DOMContentLoaded", function(e) {

  function mouse(event){
    var x = event.clientX;
    var y = event.clientY;
    var oldX = parseInt(tom.style.left.substr(0, tom.style.left.length - 2));
    var oldY = parseInt(tom.style.top.substr(0, tom.style.top.length - 2));
    var deg = Math.atan2(y-oldY, x-oldX) * 180 / 3.1415 - 180;
    tom = document.getElementById('Tom');
    tom.style.transform = "rotate(" +  deg +  "deg)";
    tom.style.left = parseInt(x-150)+'px';
    tom.style.top = parseInt(y-73)+'px';
  }

  function mouseJerry(event){
    var x = event.clientX;
    var y = event.clientY;
    jerry = document.getElementById('Jerry');
    jerry.style.left = parseInt(x-50)+'px';
    jerry.style.top = parseInt(y-50)+'px';
    //console.log(x);
  }

  document.addEventListener("click", mouse);
  document.addEventListener("mousemove", mouseJerry);
  tom = document.getElementById('Tom');
  tom.style.left = 0+'px';
  tom.style.top = 0+'px';

  jerry = document.getElementById('Jerry');
  jerry.style.left = 0+'px';
  jerry.style.top = 0+'px';

})
