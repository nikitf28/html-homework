function buttonsEvent(buttonID, listID, promtTitle, className){
  document.getElementById(buttonID).onclick = function(event){
    list = document.getElementById(listID);
    toAdd = prompt(promtTitle, '');
    if (toAdd == "" || toAdd == null){
      return;
    }
    let li = document.createElement('li');
    li.innerHTML = toAdd;
    li.classList.add(className);
    list.getElementsByTagName('ul')[0].appendChild(li);
  };
}

document.addEventListener("DOMContentLoaded", function(e) {
  buttonsEvent("bikeBtn", "bikeList", "Введите модель велосипеда", "bike");
  buttonsEvent("giroBtn", "giroList", "Введите модель гироскутера", "giro");
  buttonsEvent("monoBtn", "monoList", "Введите модель моноколеса", "mono");
  buttonsEvent("eScooterBtn", "eScooterList", "Введите модель электросамоката", "eScooter");
})
